## Particle41 DevOps Challenge -- Entry Level

This is an entry level challange to test your level of familiratiy with development and operation tools and concepts.

---

## The challenge
***

* Create a load balance solution for 2 backend microservices in any programming language of your choice.
* Backend microservices should be a webserver.
* The application should return **```hello from app1```** or **```hello from app2```** when its ```/ URL``` path is accessed. 
* Create Dockerfiles for microservices.
* Both the application should be private and should have accessed through only from loadbalancer service.


---

## Criteria

Your task will considered successful if a colleague able to deploy your solution with a single command. 

**Other criteria for evaluation will be:**
***

* Code quality and style: your code must be easy for others to read, and properly documented when relevant

* Container best practices: your container image should be as small as possiblem without unnecessary bloat


---

